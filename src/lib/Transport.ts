import { EventEmitter } from 'events';

export interface RpcCommon {
  jsonrpc: '2.0';
}

export interface RpcCall<Params extends unknown = any> extends RpcCommon {
  id: string | number | null;
  method: string;
  params?: Params;
}

export interface RpcNotify<Params extends unknown = any> extends RpcCommon {
  method: string;
  params?: Params;
}

export interface RpcResult<Result extends unknown = any> extends RpcCommon {
  id: string;
  result: Result;
}

export interface RpcError<Data extends unknown = any> extends RpcCommon {
  id: string;
  error: {
    code: number;
    message: string;
    data: Data;
  };
}

export type RpcRequest = RpcCall | RpcNotify;

export type RpcResponse = RpcResult | RpcError;

export interface Serializer<Deserialized extends unknown = any, Serialized extends unknown = any> {
  serialize(rpcPackage: Deserialized): Serialized;

  deserialize(data: Serialized): Deserialized;
}

export interface Compressor {
  compress(data: string): string;

  decompress(data: string): string;
}

export interface TransportOptions {
  serializer: Serializer;
  compressor?: Compressor;

  [key: string]: any;
}

export type Listener = ((rpcData: Request) => PromiseLike<Response> | PromiseLike<void> | Response | void) | ((rpcData: Response) => PromiseLike<void> | void);

export interface RpcTransport<Request extends unknown = any, Response extends unknown = any> {
  send(request: Request, ...args: any[]): PromiseLike<Response>;

  reply(response: Response, ...args: any[]): PromiseLike<void>;

  listen(method: string, listener: Listener, ...args: any[]): PromiseLike<void>;

  on(event: 'connect' | 'disconnect', handler: (transport: RpcTransport<Request, Response>) => any): void;

  on(event: 'error', handler: (error: Error) => any): void;

  on(event: 'rpc-request', handler: (request: RpcRequest) => any): void;

  on(event: 'rpc-call', handler: (call: RpcCall) => any): void;

  on(event: 'rpc-notify', handler: (notify: RpcNotify) => any): void;

  on(event: 'rpc-response', handler: (response: RpcResponse) => any): void;

  on(event: 'rpc-result', handler: (result: RpcResult) => any): void;

  on(event: 'rpc-error', handler: (error: RpcError) => any): void;

  on(event: string, handler: (...args: any[]) => any): void;
}

export abstract class Transport<Req extends unknown = any, Res extends unknown = any>
  extends EventEmitter
  implements RpcTransport<Req, Res> {
  protected options?: TransportOptions;

  public abstract get initialized(): boolean;

  public async init(opts: TransportOptions): Promise<Transport<Req, Res>> {
    this.options = opts;
    return this;
  }

  public abstract listen(method: string, handler: Listener): PromiseLike<void>;

  public abstract reply(response: Res): PromiseLike<void>;

  public abstract send(request: Req): PromiseLike<Res>;
}
